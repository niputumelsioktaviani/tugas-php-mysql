<?php
  // memanggil file koneksi.php untuk membuat koneksi
include 'koneksi.php';

  // mengecek apakah di url ada nilai GET id_dosen
  if (isset($_GET['id_dosen'])) {
    // ambil nilai id dari url dan disimpan dalam variabel $id_dosen
    $id_dosen = ($_GET["id_dosen"]);

    // menampilkan data dari database yang mempunyai id_dosen=$id_dosen
    $query = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if(!$result){
      die ("Query Error: ".mysqli_errno($koneksi).
         " - ".mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);      
  }
       
  ?>

<?php include "header.php";?>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link btn btn-success" href="tambahdosen.php">Tambah</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="dosen.php">Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "koneksi.php";?> <!-- Untuk menghubungkan ke database -->
            <h4>Edit Dosen dengan Id Dosen = <?php echo $data['id_dosen']; ?></h4>
            <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 border border-success mt-3 p-3">
            <form method="POST" action="editdosen.php" enctype="multipart/form-data" >
            <input name="id_dosen" value="<?php echo $data['id_dosen']; ?>"  hidden />
                <div class="mb-3">
                            <label for="fotoDosen" class="form-label">Foto Dosen</label>
                            <img src="gambar/<?php echo $data['fotoDosen']; ?>" style="width: 120px;float: left;margin-bottom: 5px;">
                            <input type="file" name="fotoDosen" class="form-control" id="fotoDosen" required="" />
                            <i style="float: left;font-size: 11px;color: red"></i>
                        </div>
                        <div class="mb-3">
                            <label for="nipDosen" class="form-label">NIP Dosen</label>
                            <input type="text" name="nipDosen" class="form-control" id="nipDosen" value="<?php echo $data['nip_dosen']; ?>" autofocus=""  required="" />
                        </div>
                        <div class="mb-3">
                            <label for="namaDosen" class="form-label">Nama Dosen</label>
                            <input type="text" name="namaDosen" class="form-control" id="namaDosen" value="<?php echo $data['nama_dosen']; ?>" autofocus=""  required="" />
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="prodiDosen" class="form-label">Prodi</label>
                                    <input type="text" name="prodiDosen" class="form-control" id="prodiDosen" value="<?php echo $data['prodi']; ?>" autofocus=""  required="" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="fakultasDosen" class="form-label">Fakultas</label>
                                    <input type="text" name="fakultasDosen" class="form-control" id="fakultasDosen" value="<?php echo $data['fakultas']; ?>" autofocus=""  required="" />
                                </div>
                            </div>
                        </div>
                        
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
        </div>
    </div>
<?php include "footer.php";?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
