<?php include "header.php";?>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link btn btn-success" href="tambahdosen.php">Tambah</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="dosen.php">Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "koneksi.php";?> <!-- Untuk menghubungkan ke database -->
            <h4>Tambah Dosen</h4>
            <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 border border-success mt-3 p-3">
            <form method="POST" action="simpandosen.php" enctype="multipart/form-data" >
                <div class="mb-3">
                            <label for="fotoDosen" class="form-label">Foto Dosen</label>
                            <input type="file" name="fotoDosen" class="form-control" id="fotoDosen" required="" />
                        </div>
                        <div class="mb-3">
                            <label for="nipDosen" class="form-label">NIP Dosen</label>
                            <input type="text" name="nipDosen" class="form-control" id="nipDosen" required="" />
                        </div>
                        <div class="mb-3">
                            <label for="namaDosen" class="form-label">Nama Dosen</label>
                            <input type="text" name="namaDosen" class="form-control" id="namaDosen" required="" />
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="prodiDosen" class="form-label">Prodi</label>
                                    <input type="text" name="prodiDosen" class="form-control" id="prodiDosen" required="" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="fakultasDosen" class="form-label">Fakultas</label>
                                    <input type="text" name="fakultasDosen" class="form-control" id="fakultasDosen" required="" />
                                </div>
                            </div>
                        </div>
                        
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
        </div>
    </div>
<?php include "footer.php";?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>