<?php include "header.php";?>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item btn btn-success">
                                <a class="nav-link" href="tambahdosen.php">Tambah</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="dosen.php">Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "koneksi.php";?> <!-- Untuk menghubungkan ke database -->
            <h4>CRUD Dosen</h4>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Id_Dosen</th>
                        <th>Foto_Dosen</th>
                        <th>NIP_Dosen</th>
                        <th>Nama Dosen</th>
                        <th>Prodi</th>
                        <th>Fakultas</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
                        $query = "SELECT * FROM dosen";
                        $result = mysqli_query($koneksi, $query);
                        //mengecek apakah ada error ketika menjalankan query
                        if(!$result){
                            die ("Query Error: ".mysqli_errno($koneksi).
                            " - ".mysqli_error($koneksi));
                        }

                        //buat perulangan untuk element tabel dari data mahasiswa
                        $id_dosen = 1; //variabel untuk membuat nomor urut
                        // hasil query akan disimpan dalam variabel $data dalam bentuk array
                        // kemudian dicetak dengan perulangan while
                        while($rs = mysqli_fetch_assoc($result))
                        {
                    ?>

                        <tr>
                            <td><?php echo $id_dosen;?></td>
                            <td style="text-align: center;"><img src="gambar/<?php echo $rs['fotoDosen'];?>" style="width: 120px;"></td>
                            <td><?php echo $rs['nip_dosen'];?></td>
                            <td><?php echo $rs['nama_dosen'];?></td>
                            <td><?php echo $rs['prodi'];?></td>
                            <td><?php echo $rs['fakultas'];?></td>
                            <td>
                                <a class="btn btn-warning" href="proseseditdosen.php?id_dosen=<?php echo $rs['id_dosen'];?>">Edit</a>
                                <a class="btn btn-danger" href="hapusdosen.php?id_dosen=<?php echo $rs['id_dosen'];?>" onclick="return confirm('Anda yakin ingin hapus data ini?')">Hapus</a>
                            </td>
                        </tr>
                    <?php
                        $id_dosen++; //untuk nomor urut terus bertambah 1
                        }
                    ?>
                    
                </tbody>
            </table>
        </div>
    </div>
<?php include "footer.php";?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
        