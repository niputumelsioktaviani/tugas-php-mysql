-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Jun 2021 pada 07.11
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si_penjadwalandosen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `id_dosen` bigint(20) NOT NULL,
  `fotoDosen` longtext NOT NULL,
  `nip_dosen` varchar(25) NOT NULL,
  `nama_dosen` varchar(255) NOT NULL,
  `prodi` varchar(255) NOT NULL,
  `fakultas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `fotoDosen`, `nip_dosen`, `nama_dosen`, `prodi`, `fakultas`) VALUES
(5, '393-darmawiguna.jpg', '198501042010121004', 'I Gede Mahendra Darmawiguna, S.Kom., M.Sc.', 'Sistem Informasi', 'Teknik dan Kejuruan'),
(6, '171-Aditra.jpg', '198901192015041004', 'Gede Aditra Pradnyana, S.Kom., M.Kom.', 'Sistem Informasi', 'Teknik dan Kejuruan'),
(8, '32-yudia.jpg', '199308042020122008', 'Putu Yudia Pratiwi, S.Pd., M.Eng.', 'Sistem Informasi', 'Teknik dan Kejuruan'),
(9, '747-diatri.jpg', '198907112020122004', 'I Gusti Ayu Agung Diatri Indradewi, S.Kom., M.T.', 'Sistem Informasi', 'Teknik dan Kejuruan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` bigint(20) NOT NULL,
  `id_dosen` bigint(20) NOT NULL,
  `id_kelas` bigint(20) NOT NULL,
  `jadwal` datetime NOT NULL,
  `mata_kuliah` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_dosen`, `id_kelas`, `jadwal`, `mata_kuliah`) VALUES
(64, 8, 205, '2021-06-20 14:44:33', 'Matematika Diskrit'),
(65, 5, 215, '2020-06-19 14:10:33', 'Pemrograman Mobile'),
(66, 6, 210, '2021-06-22 14:44:33', 'Pemrograman Web');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` bigint(20) NOT NULL,
  `nama_kelas` varchar(255) NOT NULL,
  `prodi` varchar(255) NOT NULL,
  `fakultas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `prodi`, `fakultas`) VALUES
(205, 'MIPA 1', 'Matematika', 'Matematika dan Ilmu Pengetahuan Alam'),
(208, 'SI 4A', 'Sistem Informasi', 'Teknik dan Kejuruan'),
(210, 'MI 5A', 'Manajemen Informatika', 'Teknik dan Kejuruan'),
(213, 'Olahraga 6A', 'Pendidikan Olahraga', 'Olahraga dan Kesehatan'),
(215, 'Manajemen 6C', 'S1 Manajemen', 'Ekonomi'),
(218, 'Bahasa 1', 'Pendidikan Bahasa Inggris', 'Bahasa dan Seni');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id_dosen`);

--
-- Indeks untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `id_dosen` (`id_dosen`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id_dosen` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id_dosen`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
