<?php include "header.php";?>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link btn btn-info" href="tambahkelas.php">Tambah</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="kelas.php">Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "koneksi.php";?> <!-- Untuk menghubungkan ke database -->
            <h4>Tambah Kelas</h4>
            <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 border border-info mt-3 p-3">
                <form action="simpankelas.php" method="post">
                        <div class="mb-3">
                            <label for="namaKelas" class="form-label">Nama Kelas</label>
                            <input type="text" name="namaKelas" class="form-control" id="namaKelas">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="prodi" class="form-label">Prodi</label>
                                    <input type="text" name="prodi" class="form-control" id="prodi">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="fakultas" class="form-label">Fakultas</label>
                                    <input type="text" name="fakultas" class="form-control" id="fakultas">
                                </div>
                            </div>
                        </div>
                        
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
        </div>
    </div>
<?php include "footer.php";?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>