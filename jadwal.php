<?php include "header.php";?>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item btn btn-primary">
                                <a class="nav-link" href="tambahjadwal.php">Tambah</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="jadwal.php">Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "koneksi.php";?> <!-- Untuk menghubungkan ke database -->
            <h4>CRUD Jadwal</h4>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Id_Jadwal</th>
                        <th>Id_Dosen</th>
                        <th>Id_Kelas</th>
                        <th>Jadwal</th>
                        <th>Mata Kuliah</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                        // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
                        $query = "SELECT * FROM jadwal";
                        $result = mysqli_query($koneksi, $query);
                        //mengecek apakah ada error ketika menjalankan query
                        if(!$result){
                            die ("Query Error: ".mysqli_errno($koneksi).
                            " - ".mysqli_error($koneksi));
                        }

                        //buat perulangan untuk element tabel dari data mahasiswa
                        $id_jadwal = 101; //variabel untuk membuat nomor urut
                        // hasil query akan disimpan dalam variabel $data dalam bentuk array
                        // kemudian dicetak dengan perulangan while
                        while($rs = mysqli_fetch_assoc($result))
                        {
                    ?>

                        <tr>
                            <td><?php echo $id_jadwal;?></td>
                            <td><?php echo $rs['id_dosen'];?></td>
                            <td><?php echo $rs['id_kelas'];?></td>
                            <td><?php echo $rs['jadwal'];?></td>
                            <td><?php echo $rs['mata_kuliah'];?></td>
                            <td>
                                <a class="btn btn-warning" href="proseseditjadwal.php?id_jadwal=<?php echo $rs['id_jadwal'];?>">Edit</a>
                                <a class="btn btn-danger" href="hapusjadwal.php?id_jadwal=<?php echo $rs['id_jadwal'];?>" onclick="return confirm('Anda yakin ingin hapus data ini?')">Hapus</a>
                            </td>
                        </tr>
                    <?php
                        $id_jadwal++; //untuk nomor urut terus bertambah 1
                        }
                    ?>
                    
                </tbody>
            </table>
        </div>
    </div>
<?php include "footer.php";?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>