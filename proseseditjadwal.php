<?php
  // memanggil file koneksi.php untuk membuat koneksi
include 'koneksi.php';

  // mengecek apakah di url ada nilai GET id
  if (isset($_GET['id_jadwal'])) {
    // ambil nilai id dari url dan disimpan dalam variabel $id
    $id_jadwal = ($_GET["id_jadwal"]);

    // menampilkan data dari database yang mempunyai id=$id
    $query = "SELECT * FROM jadwal WHERE id_jadwal='$id_jadwal'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if(!$result){
      die ("Query Error: ".mysqli_errno($koneksi).
         " - ".mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);      
  }
       
  ?>

<?php include "header.php";?>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link btn btn-info" href="tambahjadwal.php">Tambah</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="jadwal.php">Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "koneksi.php";?> <!-- Untuk menghubungkan ke database -->
            <h4>Edit jadwal dengan Id Jadwal = <?php echo $data['id_jadwal']; ?></h4>
            <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 border border-info mt-3 p-3">
                <form action="editjadwal.php" method="post">
                <input name="id_jadwal" value="<?php echo $data['id_jadwal']; ?>"  hidden />
                        <div class="mb-3">
                            <label for="idDosen" class="form-label">Id Dosen</label>
                            <input type="text" name="idDosen" class="form-control" id="idDosen" value="<?php echo $data['id_dosen']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="idKelas" class="form-label">Id Kelas</label>
                            <input type="text" name="idKelas" class="form-control" id="idKelas" value="<?php echo $data['id_kelas']; ?>">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="jadwalKelas" class="form-label">Jadwal Kelas</label>
                                    <input type="text" name="jadwalKelas" class="form-control" id="jadwalKelas" value="<?php echo $data['jadwal']; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="matkul" class="form-label">Mata Kuliah</label>
                                    <input type="text" name="matkul" class="form-control" id="matkul" value="<?php echo $data['mata_kuliah']; ?>">
                                </div>
                            </div>
                        </div>
                        
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
        </div>
    </div>
<?php include "footer.php";?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>